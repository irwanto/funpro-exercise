data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr | V String | Let String Expr Expr
            deriving Show

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

evaluate :: Expr -> Float
evaluate (C x) = x
evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
evaluate (e1 :- e2) = evaluate e1 - evaluate e2
evaluate (e1 :* e2) = evaluate e1 * evaluate e2
evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
evaluate (V _) = 0.0

-- merancang higher order function yang bekerja pada struktur data ekspresi
foldEvaluate (c, plus, minus, mult, div, flet, fv) (C x) = c x
foldEvaluate (c, plus, minus, mult, div, flet, fv) (e1 :+ e2) = plus (foldEvaluate (c, plus, minus, mult, div, flet, fv) e1) (foldEvaluate (c, plus, minus, mult, div, flet, fv) e2) 
foldEvaluate (c, plus, minus, mult, div, flet, fv) (e1 :- e2) = minus (foldEvaluate (c, plus, minus, mult, div, flet, fv) e1) (foldEvaluate (c, plus, minus, mult, div, flet, fv) e2) 
foldEvaluate (c, plus, minus, mult, div, flet, fv) (e1 :* e2) = mult (foldEvaluate (c, plus, minus, mult, div, flet, fv) e1) (foldEvaluate (c, plus, minus, mult, div, flet, fv) e2) 
foldEvaluate (c, plus, minus, mult, div, flet, fv) (e1 :/ e2) = div (foldEvaluate (c, plus, minus, mult, div, flet, fv) e1) (foldEvaluate (c, plus, minus, mult, div, flet, fv) e2) 
foldEvaluate (c, plus, minus, mult, div, flet, fv) (Let v e1 e2) = flet v e1 e2
foldEvaluate (c, plus, minus, mult, div, flet, fv) (V e) = fv e

-- fungsi evaluate baru dengan foldEvaluate
newEvaluate expr = foldEvaluate (c, plus, minus, mult, div, flet, fv) expr
    where
        c x = x
        plus e1 e2 = e1 + e2
        minus e1 e2 = e1 - e2
        mult e1 e2 = e1 * e2
        div e1 e2 = e1 / e2
        flet v e1 e2 = newEvaluate (subst v e1 e2)
        fv e = 0.0

-- menggunakan fold untuk mendefinisikan fungsi menghitung jumlah konstanta yang digunakan
foldConst (one, zero, flet) (C x) = one
foldConst (one, zero, flet) (e1 :+ e2) = (foldConst (one, zero, flet) e1) + (foldConst (one, zero, flet) e2)
foldConst (one, zero, flet) (e1 :- e2) = (foldConst (one, zero, flet) e1) + (foldConst (one, zero, flet) e2)
foldConst (one, zero, flet) (e1 :* e2) = (foldConst (one, zero, flet) e1) + (foldConst (one, zero, flet) e2)
foldConst (one, zero, flet) (e1 :/ e2) = (foldConst (one, zero, flet) e1) + (foldConst (one, zero, flet) e2)
foldConst (one, zero, flet) (Let v e1 e2) = flet e2
foldConst (one, zero, flet) (V e) = zero

countConst expr = foldConst (one, zero, flet) expr
    where
        one = 1
        zero = 0
        flet e2 = 1 + countConst e2

-- fungsi menghitung jumlah konstanta dengan fungsi fold yang telah dibuat sebelumnaya
countConst2 expr = foldEvaluate (c, plus, minus, mult, div, flet, fv) expr
    where
        c x = 1
        plus e1 e2 = e1 + e2
        minus e1 e2 = e1 + e2
        mult e1 e2 = e1 + e2
        div e1 e2 = e1 + e2
        flet v e1 e2 = 1 + countConst2 e2
        fv e = 0

countOperator expr = foldEvaluate (c, plus, minus, mult, div, flet, fv) expr
    where
        c x = 0
        plus e1 e2 = 1 + e1 + e2
        minus e1 e2 = 1 + e1 + e2
        mult e1 e2 = 1 + e1 + e2
        div e1 e2 = 1 + e1 + e2
        flet v e1 e2 = 0
        fv e = 0
