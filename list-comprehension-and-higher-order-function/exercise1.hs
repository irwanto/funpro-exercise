-- length function using map and sum
-- map elements of list to 1 then sum all of the elements
-- Type is customLength :: Num a1 => [a2] -> a1
customLength xs = sum (map (\x -> 1) xs)

-- map f (map g xs) means function g will be mapped to element in list xs then
-- the list produced will be mapped by function f. It is like (f o g)(x) in math.
example xs = map (+2) (map (+1) xs)

-- iter function
-- iter n f x = f (f (... (f x)))
iter :: Integer -> (a -> a) -> a -> a 
iter 0 f x = x
iter n f x = iter (n-1) f (f x)
-- iterExample type wiil be Integer -> Integer! The effect is applying succ function n times
iterExample = \n -> iter n succ 33

-- sum of the squares of [1 .. n]
sumOfSquares n = foldr (+) 0 (map (\x -> x * x) [1 .. n])

-- sing will return list of list with one element.
-- mystery will append all the list with one element together resulting to original list
mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]

-- Compose list of functions into single function
composeList :: [a -> a] -> (a -> a)
composeList [] = id
composeList (f:fs) = f . composeList fs

-- flip function which reverses the order in which its function argument takes its arguments
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f a b = f b a