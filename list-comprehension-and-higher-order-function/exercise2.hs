-- Notes:
-- xs, ys = list of integers
-- xys = list of (x, y)

-- List comprehension to HOF
plusOne_LC xs = [ x + 1 | x <- xs ]
plusOne_F xs = map (+1) xs

addXY_LC xs ys = [ x + y | x <- xs, y <- ys]
addXY_F xs ys = concat (map (\x -> map (\y -> (x + y)) ys) xs)

plusTwo_LC xs = [ x + 2 | x <- xs, x > 3 ]
plusTwo_HOF xs = map (+2) (filter (>3) xs)

plusThree_LC xys = [ x + 3 | (x, _) <- xys ]
plusThree_HOF xys = map (\(x, _) -> x + 3) xys

plusFour_LC xys = [ x + 4 | (x, y) <- xys, x + y < 5]
plusFour_HOF xys = map (\(x, y) -> x + 4) (filter (\(x, y) -> (x + y) < 5) xys)

-- Below two functions parameter is [Just Int, Just Int, ...]
plusFive_LC mxs = [ x + 5 | Just x <- mxs ]
plusFive_HOF mxs = map (\(Just x) -> x + 5) mxs

-- HOF to list comprehension
plus3_HOF xs = map (+3) xs
plus3_LC xs = [ x + 3 | x <- xs ]

gt7_HOF xs = filter (>7) xs
gt7_LC xs = [ x | x <- xs, x > 7]

cross_HOF xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)
cross_LC xs ys = [ (x,y) | x <- xs, y <- ys ]

add_HOF xys = filter (>3) (map (\(x,y) -> x+y) xys)
add_LC xys = [ x + y | (x, y) <- xys, x + y > 3]