import Data.List

divisor n = [ x | x <- [1 .. n], n `mod` x == 0]

quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y < x] ++ [x] ++ quickSort [y | y <- xs, y >= x]

permute [] = [ [] ]
permute xs = [ x:sisa | x <- xs, sisa <- permute(xs\\[x])]

pythaTriple = [ (x, y, z) | z <- [5 .. ], y <- [1 .. z], x <- [1 .. y], x*x + y*y == z*z]
pythaTriple2 = [ (x, y, z) | z <- [5 .. ], y <- [z, z-1 .. 1], x <- [y, y-1 .. 1], x*x + y*y == z*z]

primes = sieve [2 .. ]
    where
        sieve (x:xs) = x : (sieve [ y | y <- xs, y `mod` x /= 0])